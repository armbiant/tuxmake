#!/usr/bin/env python3

import argparse
import os
import sys

from tuxmake.runtime import Runtime


def main(argv=sys.argv):
    parser = argparse.ArgumentParser(
        prog="tuxdate",
        usage="%(prog)s [OPTIONS] CMD ...",
        description="Runs CMD, maybe using containerss (example tuxmake runtime library client)",
        add_help=True,
    )

    parser.add_argument(
        "-r", "--runtime", type=str, default="podman", help="Container runtime to use"
    )
    parser.add_argument(
        "-i", "--image", type=str, default="debian", help="Container image to use"
    )
    parser.add_argument(
        "-u", "--user", type=str, help="User to run container as"
    )
    parser.add_argument(
        "-g", "--group", type=str, help="Group to run container as"
    )
    parser.add_argument(
        "-I", "--interactive", action="store_true", help="Interactive (allocate a TTY)"
    )
    parser.add_argument("command", nargs="+")
    options = parser.parse_args(argv[1:])

    runtime = Runtime.get(options.runtime)
    runtime.set_image(options.image)
    if options.user:
        runtime.set_user(options.user)
    if options.group:
        runtime.set_group(options.group)
    runtime.prepare()
    runtime.run_cmd(options.command, interactive=options.interactive, offline=False)
    runtime.cleanup()


main()
